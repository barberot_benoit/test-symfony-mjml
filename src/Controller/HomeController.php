<?php

namespace App\Controller;

use App\Service\MailService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    #[Route('/', name: 'app_home')]
    public function index(MailService $mailService): Response
    {
        $mailService->sendMail();

        return $this->render('home/index.html.twig', [
            'title' => 'Home',
        ]);
    }

    #[Route('/inky', name: 'app_home_inky')]
    public function inky(MailService $mailService): Response
    {
        $mailService->sendMailWithInky();

        return $this->render('home/index.html.twig', [
            'title' => 'Inky',
        ]);
    }
}
