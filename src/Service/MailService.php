<?php

namespace App\Service;

use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\MailerInterface;

class MailService
{
    private $mailer;

    public function __construct(MailerInterface $mailer)
    {
        $this->mailer = $mailer;
    }

    public function sendMail()
    {
        $email = (new TemplatedEmail())
            ->from('test@mail.com')
            ->to('testto@mail.to')
            ->subject('test')
            ->htmlTemplate('mails/test.mjml.twig')
            ->context([
                'username' => 'Benoit',
            ]);

        $this->mailer->send($email);
    }

    public function sendMailWithInky()
    {
        $email = (new TemplatedEmail())
            ->from('test@mail.inky')
            ->to('test@inky.mailto')
            ->subject('test inky')
            ->htmlTemplate('mails/test.inky.twig')
            ->context([
                'username' => 'Benoit',
            ]);

        $this->mailer->send($email);
    }
}
